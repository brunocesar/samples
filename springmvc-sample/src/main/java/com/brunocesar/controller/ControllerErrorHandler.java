package com.brunocesar.controller;

import java.util.logging.Logger;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerErrorHandler {

    private static final Logger LOGGER = Logger.getLogger(ControllerErrorHandler.class.getName());

    @ExceptionHandler(Exception.class)
    public String processException(final Exception ex) {
        LOGGER.info("ControllerErrorHandler#processException --> error.jsp");
        return "error";
    }

}

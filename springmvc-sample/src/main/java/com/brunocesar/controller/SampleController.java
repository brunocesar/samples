package com.brunocesar.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/test")
public class SampleController {

    private static final Logger LOGGER = Logger.getLogger(SampleController.class.getName());

    @RequestMapping(method = GET)
    public final String get() throws Exception {
        LOGGER.info("SampleController#get --> calling get to throw Exception");
        throw new Exception("This is the error!");
    }

}

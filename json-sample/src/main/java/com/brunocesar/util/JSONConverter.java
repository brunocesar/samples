package com.brunocesar.util;

import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class JSONConverter {

    private JSONConverter() {}

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
    }

    public static <T> T fromJson(final String json, final Class<T> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (final IOException e) {
            throw new IllegalArgumentException("Error on JSON parse to object: \n" + json, e);
        }
    }

    public static String toJson(final Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (final IOException e) {
            throw new IllegalArgumentException("Error on JSON parse from object: " + object, e);
        }
    }

}

package com.brunocesar.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;

/**
 * Utilities for files
 *
 * @author Bruno César - <a href="mailto:bruno@brunocesar.com">bruno@brunocesar.</a>
 * @since 11/01/2015
 */
public final class FileUtil {

    /**
     * Retrieves as {@link String} the file content
     *
     * @param fileName
     *            file name in classpath
     * @return {@link String} do conteúdo
     */
    public static String getFileContent(final String fileName) {
        String fileContent;
        try (final InputStream is = FileUtil.class.getResourceAsStream(fileName)) {
            if (is == null) {
                throw new RuntimeException("File '" + fileName + "' can't be found.");
            }
            fileContent = FileUtil.getConteudoFromInputStream(is);
        } catch (final IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return fileContent;
    }

    /**
     * Retrives the {@link InputStream} content in a {@link String}
     *
     * @param inputStream
     *            {@link InputStream} to be read as a {@link String}
     * @return {@link String} with the {@link InputStream} content
     * @throws IOException
     *             in case of an {@code I/O} error
     */
    public static String getConteudoFromInputStream(final InputStream inputStream) throws IOException {
        String content = null;
        try (InputStreamReader in = new InputStreamReader(inputStream, Charset.defaultCharset());
                StringWriter sw = new StringWriter();) {
            final char[] buffer = new char[4096];
            int n = 0;
            while (-1 != (n = in.read(buffer))) {
                sw.write(buffer, 0, n);
            }
            content = sw.toString();
        } catch (final IOException e) {
            throw new IOException("Problema ao recuperar contéudo do arquivo em string: " + e.getMessage());
        }
        return content;
    }

}

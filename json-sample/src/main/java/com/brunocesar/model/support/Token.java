package com.brunocesar.model.support;

import lombok.Getter;
import lombok.Setter;

public class Token {

    @Getter
    @Setter
    private String tokenName;

    @Getter
    @Setter
    private String startIndex;

    @Getter
    @Setter
    private String lastIndex;

}

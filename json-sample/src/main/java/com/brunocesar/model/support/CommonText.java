package com.brunocesar.model.support;

import lombok.Getter;
import lombok.Setter;

public class CommonText {

    @Getter
    @Setter
    private String text;

}

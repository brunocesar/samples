package com.brunocesar.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.brunocesar.model.support.CommonText;
import com.brunocesar.model.support.Token;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CUSTOM, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@Type(value = USBChannel.class, name = "USB"), @Type(value = SerialChannel.class, name = "Serial")})
public class Channel {

    @Getter
    @Setter
    private CommonText code;

    @Getter
    @Setter
    private String baudRate;

    @Getter
    @Setter
    private String dataLength;

    @Getter
    @Setter
    private String stopBit;

    @Getter
    @Setter
    private String parity;

    @Getter
    @Setter
    private String communicationFormat;

    @Getter
    @Setter
    @JsonProperty(value = "token")
    List<Token> tokens;

}

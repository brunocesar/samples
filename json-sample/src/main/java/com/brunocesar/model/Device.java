package com.brunocesar.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.brunocesar.model.support.CommonText;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Device {

    @Getter
    @Setter
    private String resourceType;

    @Getter
    @Setter
    private CommonText type;

    @Getter
    @Setter
    private String manufacturer;

    @Getter
    @Setter
    @JsonProperty(value = "channel")
    private List<Channel> channels;

}

package com.brunocesar.model;

import lombok.Getter;
import lombok.Setter;

public class SerialChannel extends Channel {

    @Getter
    @Setter
    private String port;

}

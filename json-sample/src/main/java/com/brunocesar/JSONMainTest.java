package com.brunocesar;

import com.brunocesar.model.Device;
import com.brunocesar.util.FileUtil;
import com.brunocesar.util.JSONConverter;

public class JSONMainTest {

    public static void main(String[] args) {
        final String jsonContent = FileUtil.getFileContent("/jsons/json-1.json");
        final Device device = JSONConverter.fromJson(jsonContent, Device.class);
        System.out.println("Channel class name: " + device.getChannels().get(0).getClass().getName());
        System.out.println("resourceType: " + device.getResourceType());
        System.out.println("type: " + device.getType().getText());
        System.out.println("manufacturer: " + device.getManufacturer());
    }

}

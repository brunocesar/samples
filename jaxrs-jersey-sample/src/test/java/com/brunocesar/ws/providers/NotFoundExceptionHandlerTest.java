package com.brunocesar.ws.providers;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Test;

import com.brunocesar.exception.NotFoundException;
import com.brunocesar.ws.providers.NotFoundExceptionHandler.ErrorMessage;

public class NotFoundExceptionHandlerTest {

    private static final String EXCEPTION_MESSAGE = "Exception message";

    @Test
    public void testToResponse() {
        final NotFoundExceptionHandler handler = new NotFoundExceptionHandler();
        final Response response = handler.toResponse(new NotFoundException(EXCEPTION_MESSAGE));

        final ErrorMessage message = new ErrorMessage(EXCEPTION_MESSAGE);

        Assert.assertEquals(message, response.getEntity());
        Assert.assertEquals(message.getError(), EXCEPTION_MESSAGE);
    }

}

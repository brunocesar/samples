package com.brunocesar.ws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.util.runner.ConcurrentRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.brunocesar.bean.User;

@RunWith(ConcurrentRunner.class)
public class UserWSTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new com.brunocesar.Application();
    }

    @Test
    public void testSave() {
        final User user = User.builder().login("novousuario").password("senha").build();
        final Entity<User> entity = Entity.entity(user, MediaType.APPLICATION_JSON);
        final User saved = this.target().path("user").request(MediaType.APPLICATION_JSON).post(entity)
                .readEntity(User.class);
        assertNotNull(saved.getId());
        assertEquals("novousuario", saved.getLogin());
        assertEquals("senha", saved.getPassword());
        assertTrue(saved.isActive());
    }

    @Test
    public void testFindById() {
        final User user = this.target().path("user/1").request(MediaType.APPLICATION_JSON).get(User.class);
        assertEquals(1, user.getId());
        assertEquals("user.one", user.getLogin());
        assertTrue(user.isActive());
    }

    @Test
    public void testFindByIdNotFound() {
        final Response response = this.target().path("user/10").request(MediaType.APPLICATION_JSON).get();
        assertTrue(response.getStatus() == Status.NOT_FOUND.getStatusCode());
    }

    @Test
    public void testFindByLogin() {
        final User user = this.target().path("user/login/user.one").request(MediaType.APPLICATION_JSON).get(User.class);
        assertEquals(1, user.getId());
        assertEquals("user.one", user.getLogin());
        assertTrue(user.isActive());
    }

    @Test
    public void testFindByLoginUpperCase() {
        final User user = this.target().path("user/login/USER.TWO").request(MediaType.APPLICATION_JSON).get(User.class);
        assertEquals(2, user.getId());
        assertEquals("user.two", user.getLogin());
        assertTrue(user.isActive());
    }

    @Test
    public void testFindByLoginNotFound() {
        final Response response = this.target().path("user/login/notfound").request(MediaType.APPLICATION_JSON).get();
        assertTrue(response.getStatus() == Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testListAll() {
        final List<User> users = this.target().path("user").request(MediaType.APPLICATION_JSON).get(List.class);
        assertTrue(users.size() > 0);
    }

}

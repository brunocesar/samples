package com.brunocesar.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import com.brunocesar.bean.User;
import com.brunocesar.exception.NotFoundException;
import org.junit.BeforeClass;
import org.junit.Test;

import com.brunocesar.data.impl.UserDAOImpl;

public final class UserDAOTest {

    private static UserDAO DAO;

    @BeforeClass
    public static void setUpTest() {
        DAO = UserDAOImpl.newInstance();
    }

    @Test
    public void testSave() {
        final User user = User.builder().login("bruno.cesar").password("123456").active(true).build();
        final User savedUser = DAO.save(user);
        assertEquals(4, savedUser.getId());
        assertEquals("bruno.cesar", savedUser.getLogin());
        assertEquals("123456", savedUser.getPassword());
        assertTrue(savedUser.isActive());
    }

    @Test
    public void testFindById() {
        final User result = DAO.findById(1);
        assertEquals(1, result.getId());
        assertEquals("user.one", result.getLogin());
        assertTrue(result.isActive());
    }

    @Test(expected = NotFoundException.class)
    public void testFindByIdNotFound() {
        DAO.findById(10);
    }

    @Test
    public void testFindByLogin() {
        final User result = DAO.findByLogin("USER.TWO");
        assertEquals(2, result.getId());
        assertEquals("user.two", result.getLogin());
    }

    @Test(expected = NotFoundException.class)
    public void testFindByLoginNotFound() {
        DAO.findByLogin("no.ecxist");
    }

    @Test
    public void testListAll() {
        final List<User> users = DAO.listAll();
        assertTrue(users.size() > 0);
    }

}

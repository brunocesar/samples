package com.brunocesar.data.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

import com.brunocesar.bean.User;
import com.brunocesar.data.UserDAO;
import com.brunocesar.exception.NotFoundException;

/**
 * {@link UserDAO} implementation
 *
 * @author bruno.cesar - <a href="mailto:bruno.cesar@sc.senai.br>bruno.cesar@sc.senai.br</a>
 * @since 14/07/2015
 */
public class UserDAOImpl implements UserDAO {

    private static UserDAO INSTANCE;

    private final Map<Long, User> users = new TreeMap<>();

    private final AtomicLong atomicLong = new AtomicLong(4);

    private static final String TEMPLATE_MESSAGE = "Usuário de %s '%s' não encontrado";

    private UserDAOImpl() {
        this.populateUsers();
    }

    /**
     * Creates a new {@link UserDAO} instance
     *
     * @return {@link UserDAO} instance
     */
    public static UserDAO newInstance() {
        if (INSTANCE == null) {
            synchronized (UserDAOImpl.class) {
                INSTANCE = new UserDAOImpl();
            }
        }
        return INSTANCE;
    }

    @Override
    public User save(final User user) {
        final long id = this.atomicLong.getAndIncrement();
        final User newUser = User.builder().id(id).login(user.getLogin().trim()).password(user.getPassword()).active(true).build();
        this.users.put(id, newUser);
        return newUser;
    }

    @Override
    public User findById(final long id) {
        final User result = this.users.get(id);
        if (result == null || !result.isActive()) {
            throw new NotFoundException(String.format(TEMPLATE_MESSAGE, "id", id));
        }
        return result;
    }

    @Override
    public User findByLogin(final String login) {
        final List<User> users = this.listAll();

        final String loginTrim = login.trim();

        User result = null;

        for (final User user : users) {
            if (loginTrim.equalsIgnoreCase(user.getLogin()) && user.isActive()) {
                result = user;
                break;
            }
        }

        if (result == null) {
            throw new NotFoundException(String.format(TEMPLATE_MESSAGE, "login", login));
        }
        return result;
    }

    @Override
    public List<User> listAll() {
        final List<User> users = new ArrayList<>();
        users.addAll(this.users.values());
        return users;
    }

    private void populateUsers() {
        final User userOne = User.builder().id(1).login("user.one").password("aofhdohfdoi").active(true).build();
        this.users.put(userOne.getId(), userOne);

        final User userTwo = User.builder().id(2).login("user.two").password("ashrhff").active(true).build();
        this.users.put(userTwo.getId(), userTwo);

        final User userThree = User.builder().id(3).login("user.one").password("dgdgdgadg").active(true).build();
        this.users.put(userThree.getId(), userThree);
    }

}

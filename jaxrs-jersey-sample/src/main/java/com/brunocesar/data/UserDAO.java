package com.brunocesar.data;

import java.util.List;

import com.brunocesar.bean.User;

/**
 * DAO to retrieve {@link User}
 *
 * @author bruno.cesar - <a href="mailto:bruno.cesar@sc.senai.br>bruno.cesar@sc.senai.br</a>
 * @since 14/07/2015
 */
public interface UserDAO {

    /**
     * Persists an {@link User} in a in-memory database
     *
     * @param user
     *         {@link User} to be persisted
     *
     * @return {@link User} persisted
     */
    User save(final User user);

    /**
     * Find {@link User} by your id
     *
     * @param id
     *         {@link User} id to be search
     *
     * @return {@link User}, if found
     */
    User findById(final long id);

    /**
     * Find {@link User} by your login
     *
     * @param login
     *         {@link User} login to be search
     *
     * @return {@link User}, if found
     */
    User findByLogin(final String login);

    /**
     * List all active {@link User} stored
     *
     * @return {@link List<User>}, if there are {@link User}
     */
    List<User> listAll();

}

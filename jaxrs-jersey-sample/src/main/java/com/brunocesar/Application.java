package com.brunocesar;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * JAX-RS application configuration
 *
 * @author bruno.cesar - <a href="mailto:bruno.cesar@sc.senai.br>bruno.cesar@sc.senai.br</a>
 * @since 14/07/2015
 */
@ApplicationPath("ws")
public class Application extends ResourceConfig {

    public Application() {
        this.packages("com.brunocesar.ws");
    }

}

package com.brunocesar.exception;

/**
 * Exception when an user is not found
 *
 * @author bruno.cesar - <a href="mailto:bruno.cesar@sc.senai.br>bruno.cesar@sc.senai.br</a>
 * @since 14/07/2015
 */
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 2010216080905471826L;

    /**
     * Constructs a new {@link NotFoundException}
     *
     * @param message
     *         the custom message for exception
     */
    public NotFoundException(final String message) {
        super(message);
    }

}

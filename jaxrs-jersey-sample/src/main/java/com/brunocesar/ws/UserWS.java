package com.brunocesar.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.brunocesar.bean.User;
import com.brunocesar.data.UserDAO;
import com.brunocesar.data.impl.UserDAOImpl;

/**
 * Web Services to retrieve {@link User} data. JSON only
 *
 * @author bruno.cesar - <a href="mailto:bruno.cesar@sc.senai.br>bruno.cesar@sc.senai.br</a>
 * @since 14/07/2015
 */
@Path("/user")
public class UserWS {

    private final UserDAO dao = UserDAOImpl.newInstance();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(final User user) {
        final User savedUser = this.dao.save(user);
        return Response.ok(savedUser).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAll() {
        return Response.ok(this.dao.listAll()).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") final long id) {
        return Response.ok(this.dao.findById(id)).build();
    }

    @GET
    @Path("/login/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByLogin(@PathParam("login") final String login) {
        return Response.ok(this.dao.findByLogin(login)).build();
    }

}

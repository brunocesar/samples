package com.brunocesar.ws.providers;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.brunocesar.exception.NotFoundException;

/**
 * {@link ExceptionMapper} for {@link NotFoundException}
 *
 * @author bruno.cesar - <a href="mailto:bruno.cesar@sc.senai.br>bruno.cesar@sc.senai.br</a>
 * @since 14/07/2015
 */
@Provider
public class NotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(final NotFoundException exception) {
        return Response.status(Status.NOT_FOUND).entity(new ErrorMessage(exception.getMessage()))
                .type(MediaType.APPLICATION_JSON).build();
    }

    /**
     * Simple class to encapsulate error message
     *
     * @author bruno.cesar - <a href="mailto:bruno.cesar@sc.senai.br>bruno.cesar@sc.senai.br</a>
     * @since 14/07/2015
     */
    public static class ErrorMessage {

        private final String error;

        public ErrorMessage(final String error) {
            this.error = error;
        }

        public String getError() {
            return this.error;
        }

        @Override
        public boolean equals(final Object obj) {
            if (!obj.getClass().isAssignableFrom(ErrorMessage.class)) {
                return false;
            }

            final ErrorMessage objMessage = (ErrorMessage) obj;

            return objMessage.getError().equalsIgnoreCase(this.getError());
        }

    }

}

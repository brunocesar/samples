package com.brunocesar.listener;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.brunocesar.data.ConnectionFactory;
import com.brunocesar.data.PersistenceUtil;

@WebListener
public class StartupContextListener implements ServletContextListener {

    private static Logger LOGGER = Logger.getLogger("SENAI-SAMPLE");

    @Override
    public void contextInitialized(final ServletContextEvent sce) {
        final ServletContext ctx = sce.getServletContext();
        LOGGER.info("Iniciando SENAI Sample");

        boolean success = false;
        try {
            success = PersistenceUtil.createDatabase("senai_sample");

            final Connection connection = ConnectionFactory.getDefaultConnection();

            success = PersistenceUtil.createSequence("pessoa_seq_id", connection);
            success = PersistenceUtil.createTable(this.pessoaTableDefinition(), connection);

            connection.close();
        } catch (final SQLException e) {
            LOGGER.log(Level.SEVERE, "Problema ao iniciar aplicação: " + e.getMessage());
            e.printStackTrace();
        }

        if (!success) {
            LOGGER.info("SENAI Sample inicializado com sucesso: " + ctx.getVirtualServerName() + ctx.getContextPath());
        } else {
            LOGGER.info("Problema ao inicializar SENAI Sample inicializado.");
        }
    }

    private String pessoaTableDefinition() {
        final StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE pessoas");
        sql.append("(");
        sql.append("  id bigint NOT NULL,");
        sql.append("  nome character varying(300),");
        sql.append("  CONSTRAINT pessoa_id_pk PRIMARY KEY (id)");
        sql.append(");");
        return sql.toString();
    }

    @Override
    public void contextDestroyed(final ServletContextEvent sce) {
        // do nothing
    }

}

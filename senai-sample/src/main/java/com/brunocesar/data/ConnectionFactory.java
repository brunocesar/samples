package com.brunocesar.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.postgresql.Driver;

public final class ConnectionFactory {

    private static Logger LOGGER = Logger.getLogger("SENAI-SAMPLE");

    private static final String DATABASE_URL_PATTERN = "jdbc:postgresql://localhost:5432/%s";

    private static final String DATABASE_USERNAME = "postgres";
    private static final String DATABASE_PASSWORD = "postgres";

    private ConnectionFactory() {}

    static {
        try {
            DriverManager.registerDriver(new Driver());
        } catch (final SQLException e) {
            LOGGER.log(Level.SEVERE, "Problema ao registrar driver do PostgreSQL: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static Connection getDefaultConnection() throws SQLException {
        return getConnection("senai_sample", DATABASE_PASSWORD, DATABASE_USERNAME);
    }

    public static Connection getConnection(final String database, final String password, final String username) throws SQLException {
        return DriverManager.getConnection(String.format(DATABASE_URL_PATTERN, database), password, username);
    }

}

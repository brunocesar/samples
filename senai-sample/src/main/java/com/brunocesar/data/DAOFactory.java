package com.brunocesar.data;

import com.brunocesar.data.dao.IPessoaDAO;
import com.brunocesar.data.dao.PessoaDAO;

public final class DAOFactory {

    private static IPessoaDAO pessoaDAO;

    public static IPessoaDAO pessoaDAO() {
        if (pessoaDAO == null) {
            pessoaDAO = new PessoaDAO();
        }
        return pessoaDAO;
    }

}

package com.brunocesar.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.brunocesar.data.ConnectionFactory;
import com.brunocesar.data.PersistenceUtil;
import com.brunocesar.model.Pessoa;

public class PessoaDAO implements IPessoaDAO {

    private static final String LIST_ALL_PESSOA = "SELECT * FROM PESSOAS;";
    private static final String SQL_CREATE_PESSOA = "INSERT INTO PESSOAS VALUES (?, ?);";

    @Override
    public List<Pessoa> list() throws SQLException {
        final List<Pessoa> pessoas = new ArrayList<>();

        try (final Connection connection = ConnectionFactory.getDefaultConnection();
                final PreparedStatement ps = connection.prepareStatement(LIST_ALL_PESSOA);
                final ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                final Pessoa pessoa = new Pessoa();
                pessoa.setId(rs.getLong("id"));
                pessoa.setNome(rs.getString("nome"));
                pessoas.add(pessoa);
            }
        }

        return pessoas;
    }

    @Override
    public Pessoa create(final Pessoa pessoa) throws SQLException {
        final long id = PersistenceUtil.nextValFromSequence("pessoa_seq_id");
        pessoa.setId(id);

        try (final Connection connection = ConnectionFactory.getDefaultConnection();
                final PreparedStatement ps = connection.prepareStatement(SQL_CREATE_PESSOA)) {
            ps.setLong(1, pessoa.getId());
            ps.setString(2, pessoa.getNome());
            ps.execute();
        }

        return pessoa;
    }

}

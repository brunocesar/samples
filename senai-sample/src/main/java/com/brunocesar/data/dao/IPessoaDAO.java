package com.brunocesar.data.dao;

import java.sql.SQLException;
import java.util.List;

import com.brunocesar.model.Pessoa;

public interface IPessoaDAO {

    List<Pessoa> list() throws SQLException;

    Pessoa create(final Pessoa pessoa) throws SQLException;

}

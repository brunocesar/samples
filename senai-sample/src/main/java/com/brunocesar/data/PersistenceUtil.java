package com.brunocesar.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

public final class PersistenceUtil {

    private static Logger LOGGER = Logger.getLogger("SENAI-SAMPLE");

    private static final String POSTGRES = "postgres";
    private static final String DATABASE_CREATE_PATTERN = "CREATE DATABASE %s";
    private static final String SEQUENCE_CREATE_PATTERN = "CREATE SEQUENCE %s INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;";
    private static final String SEQUENCE_NEXT_VAL_PATTERN = "sELECT NEXTVAL('%s') AS NEXT;";

    private static final String HAS_DATABASE_PATTERN = "SELECT 1 FROM pg_database WHERE datname = '%s'";
    private static final String HAS_OBJECT_PATTERN = "SELECT 1 FROM pg_class where relname = '%s'";

    private PersistenceUtil() {}

    public static boolean createDatabase(final String dataBaseName) throws SQLException {
        boolean result = true;

        final Connection connection = ConnectionFactory.getConnection(POSTGRES, POSTGRES, POSTGRES);
        final boolean hasDB = hasDatabase(dataBaseName, connection);
        if (hasDB) {
            LOGGER.info(String.format("Database '%s' já presente no servidor não será criado", dataBaseName));
        } else {
            LOGGER.info(String.format("Database '%s' não presente no servidor será criado", dataBaseName));
            result = executeQuery(String.format(DATABASE_CREATE_PATTERN, dataBaseName), connection);
        }

        return result;
    }

    public static boolean createTable(final String tableDefinition, final Connection connection) throws SQLException {
        final String tableName = "pessoas";

        boolean result = true;
        final boolean hasDB = hasObject(tableName, connection);
        if (hasDB) {
            LOGGER.info(String.format("Tabela '%s' já presente no servidor não será criada", tableName));
        } else {
            LOGGER.info(String.format("Tabela '%s' não presente no servidor será criada", tableName));
            result = executeQuery(tableDefinition, connection);
        }
        return result;
    }

    public static boolean createSequence(final String sequenceName, final Connection connection) throws SQLException {
        boolean result = true;
        final boolean hasDB = hasObject(sequenceName, connection);
        if (hasDB) {
            LOGGER.info(String.format("Sequence '%s' já presente no servidor não será criada", sequenceName));
        } else {
            LOGGER.info(String.format("Sequence '%s' não presente no servidor será criada", sequenceName));
            result = executeQuery(String.format(SEQUENCE_CREATE_PATTERN, sequenceName), connection);
        }
        return result;
    }

    public static long nextValFromSequence(final String sequenceName) throws SQLException {
        Long result = 0L;
        try (final Connection connection = ConnectionFactory.getDefaultConnection();
                final PreparedStatement ps = connection.prepareStatement(String.format(SEQUENCE_NEXT_VAL_PATTERN, sequenceName));
                final ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                result = rs.getLong("next");
            }
        }
        return result;
    }

    private static boolean executeQuery(final String query, final Connection connection) throws SQLException {
        boolean created = false;
        try (final PreparedStatement ps = connection.prepareStatement(query)) {
            created = ps.execute();
        }
        return created;
    }

    private static boolean hasDatabase(final String dataBaseName, final Connection connection) throws SQLException {
        boolean result = false;
        try (final PreparedStatement ps = connection.prepareStatement(String.format(HAS_DATABASE_PATTERN, dataBaseName));
                final ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                result = true;
            }
        }
        return result;
    }

    private static boolean hasObject(final String objectName, final Connection connection) throws SQLException {
        boolean result = false;
        try (final PreparedStatement ps = connection.prepareStatement(String.format(HAS_OBJECT_PATTERN, objectName)); final ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                result = true;
            }
        }
        return result;
    }

}

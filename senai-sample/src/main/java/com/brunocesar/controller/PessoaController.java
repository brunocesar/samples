package com.brunocesar.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.brunocesar.data.DAOFactory;
import com.brunocesar.data.dao.IPessoaDAO;
import com.brunocesar.model.Pessoa;

@WebServlet(urlPatterns = "/pessoa")
public class PessoaController extends HttpServlet {

    private static final long serialVersionUID = 4461538426754408658L;

    private static Logger LOGGER = Logger.getLogger("SENAI-SAMPLE");

    private final IPessoaDAO dao = DAOFactory.pessoaDAO();

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final RequestDispatcher dispatcher = req.getRequestDispatcher("pessoa/new.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String nome = req.getParameter("nome");

        final Pessoa pessoa = new Pessoa();
        pessoa.setNome(nome);

        String path = "pessoa/success.jsp";

        try {
            dao.create(pessoa);
            req.setAttribute("pessoa", pessoa);
        } catch (final SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
            req.setAttribute("ex", e);
            path = "pessoa/error.jsp";
        }

        final RequestDispatcher dispatcher = req.getRequestDispatcher(path);
        dispatcher.forward(req, resp);
    }

}

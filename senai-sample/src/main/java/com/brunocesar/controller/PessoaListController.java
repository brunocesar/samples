package com.brunocesar.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.brunocesar.data.DAOFactory;
import com.brunocesar.data.dao.IPessoaDAO;
import com.brunocesar.model.Pessoa;

@WebServlet(urlPatterns = "/pessoaList")
public class PessoaListController extends HttpServlet {

    private static final long serialVersionUID = 4461538426754408658L;

    private static Logger LOGGER = Logger.getLogger("SENAI-SAMPLE");

    private final IPessoaDAO dao = DAOFactory.pessoaDAO();

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        List<Pessoa> pessoas = new ArrayList<>();
        try {
            pessoas = dao.list();
        } catch (final SQLException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        req.setAttribute("pessoas", pessoas);

        final RequestDispatcher dispatcher = req.getRequestDispatcher("pessoa/list.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }

}

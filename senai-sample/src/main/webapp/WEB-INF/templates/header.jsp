<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request" />

<!DOCTYPE html>
<html lang="pt">
<head>
    <c:choose>
        <c:when test="${not empty pagename}">
            <title>SENAI Sample | ${pagename}</title>
        </c:when>
        <c:otherwise>
            <title>SENAI Sample</title>
        </c:otherwise>
    </c:choose>

    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.96.0/css/materialize.min.css" media="screen, projection" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <style type="text/css">
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        nav .nav-wrapper {
            padding: 0 30px;
        }
    </style>
</head>

<body>
    <header class="navbar-fixed">
        <nav class="indigo darken-4">
            <div class="nav-wrapper">
                <a href="${ctx}" class="brand-logo" title="SENAI Sample">SENAI Sample</a>
            </div>
        </nav>
    </header>

<%@include file="/WEB-INF/templates/taglibs.jsp" %>

<c:set var="pagename" value="Pessoas" scope="request" />

<%@include file="/WEB-INF/templates/header.jsp" %>

    <main>
        <div class="container">
            <div class="section">
                <h4>Pessoas</h4>
            </div>

            <div class="section">
                <table class="striped responsive-table">
                    <thead>
                        <tr>
                            <th data-field="id">ID</th>
                            <th data-field="nome">Nome</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${fn:length(pessoas) gt 0}">
                                <c:forEach var="pessoa" items="${pessoas}">
                                    <tr>
                                        <td>${pessoa.id}</td>
                                        <td>${pessoa.nome}</td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td colspan="2" class="center-align">N�o existem dados a serem exibidos.</td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
            <a href="${ctx}/pessoa/new.jsp" class="btn-floating btn-large waves-effect waves-light red right"><i class="mdi-content-add"></i></a>
        </div>
    </main>

<%@include file="/WEB-INF/templates/footer.jsp" %>

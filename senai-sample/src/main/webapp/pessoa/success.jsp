<%@include file="/WEB-INF/templates/taglibs.jsp" %>

<c:set var="pagename" value="Pessoas" scope="request" />

<%@include file="/WEB-INF/templates/header.jsp" %>

    <main>
        <div class="container">
            <div class="section">
                <h4>Pessoa cadastrada com sucesso: "${pessoa.nome}"</h4>
            </div>
        </div>

        <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
            <a href="${ctx}/pessoa" class="btn-floating btn-large waves-effect waves-light red right"><i class="mdi-content-add"></i></a>
        </div>
    </main>

<%@include file="/WEB-INF/templates/footer.jsp" %>

<%@include file="/WEB-INF/templates/taglibs.jsp" %>

<c:set var="pagename" value="Nova Pessoa" scope="request" />

<%@include file="/WEB-INF/templates/header.jsp" %>

    <main>
        <div class="container">
            <div class="section">
                <h4>Cadastre uma nova pessoa</h4>
            </div>

            <div class="section">
                <form id="form-mail" class="col s12" action="${ctx}/pessoa" method="post">
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="nome" name="nome" type="text" class="validate" maxlength="30" required>
                            <label for="nome">Nome*</label>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit">Salvar
                        <i class="mdi-content-send right"></i>
                    </button>
                </form>
            </div>
        </div>
    </main>

<%@include file="/WEB-INF/templates/footer.jsp" %>

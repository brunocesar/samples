<%@include file="/WEB-INF/templates/taglibs.jsp" %>

<c:set var="pagename" value="Home" scope="request" />

<%@include file="/WEB-INF/templates/header.jsp" %>

    <main>
        <div class="container">
            <div class="row center-align">
                <div class="col  s12 m6">
                    <a href="${ctx}/pessoa"><h4>Cadastrar Nova Pessoa</h4></a>
                </div>
                <div class="col s12 m6">
                    <a href="${ctx}/pessoaList"><h4>Listar Pessoas</h4></a>
                </div>
            </div>
        </div>

        <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
            <a href="${ctx}/pessoa" class="btn-floating btn-large waves-effect waves-light red right"><i class="mdi-content-add"></i></a>
        </div>
    </main>

<%@include file="/WEB-INF/templates/footer.jsp" %>